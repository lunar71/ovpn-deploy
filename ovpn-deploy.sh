#!/bin/bash
# -*- coding: utf-8 -*-

#set -x
exec > >(tee -i ~/ovpn-deploy.log)
exec 2>&1

OPENVPN_ROOT="/etc/openvpn"
OPENVPN_SERVER_ROOT="${OPENVPN_ROOT}/server"
OPENVPN_EASYRSA_ROOT="${OPENVPN_SERVER_ROOT}/easy-rsa"
EASYRSA_RELEASE='https://github.com/OpenVPN/easy-rsa/releases/download/v3.1.1/EasyRSA-3.1.1.tgz'

[[ $EUID -ne 0 ]] && printf "[-] Run as root\n" && exit 1

function setup_notifications() {
    cat > "${OPENVPN_SERVER_ROOT}/notify.sh" << "EOF"
#!/bin/bash
# -*- coding: utf-8 -*-

data=$(cat << EOL
Hostname: $(hostname)
Name: $common_name
Remote IP: $trusted_ip
Time: $time_ascii
EOL
)

case "${script_type}" in
    "client-connect")
        output="*VPN connect*\\n\`\`\`${data}\`\`\`";;
    "client-disconnect")
        output="*VPN disconnect*\\n\`\`\`${data}\`\`\`";;
    *)  
        output="VPN unknown (unknown \$script_type)";;
esac

/usr/bin/curl -sS -o /dev/null-X POST -H 'Authorization: Bearer xoxb-a-b-c' -H 'Content-Type: application/json' -d "{\"channel\": \"CHANNEL_CHANGEME\",\"blocks\":[{\"type\":\"section\",\"text\":{\"type\":\"mrkdwn\",\"text\":\"${output}\"}}]}" https://slack.com/api/chat.postMessage
EOF
    chmod +x "${OPENVPN_SERVER_ROOT}/notify.sh"
    printf "[*] Edit \"${OPENVPN_SERVER_ROOT}/notify.sh\" and \"${OPENVPN_SERVER_ROOT}/server.conf\" to enable Slack connect/disconnect notifications\n"
}

function new_client() {
    client="$1"
    [[ -e "${OPENVPN_EASYRSA_ROOT}/pki/issued/${client}.crt" ]] && printf "[-] Client name already exists, specify different name\n" && exit 2
    cd "${OPENVPN_EASYRSA_ROOT}"
    date -u >>"${OPENVPN_EASYRSA_ROOT}/install.log"
    ./easyrsa --batch --days=3650 build-client-full "$client" nopass 2>>"${OPENVPN_EASYRSA_ROOT}/install.log" 1>&2

    read server_name server_port <<< $(grep -i 'remote ' "${OPENVPN_SERVER_ROOT}/client-common.txt" | awk '{print $2" "$3}')
    client_name="${client}_${server_name}-${server_port}.ovpn"
    cat > "/root/${client_name}" <<EOF
# ${client_name}
$(cat /etc/openvpn/server/client-common.txt)
<ca>
$(cat /etc/openvpn/server/easy-rsa/pki/ca.crt)
</ca>
<cert>
$(sed -ne '/BEGIN CERTIFICATE/,$ p' /etc/openvpn/server/easy-rsa/pki/issued/$client.crt)
</cert>
<key>
$(cat /etc/openvpn/server/easy-rsa/pki/private/$client.key)
</key>
<tls-crypt>
$(sed -ne '/BEGIN OpenVPN Static key/,$ p' /etc/openvpn/server/tc.key)
</tls-crypt>
EOF
    printf "[*] Generated new client\n"
    printf "[*] ovpn connection file is /root/${client_name}\n"
}

function revoke_client() {
    client="$1"
    [[ ! -e "${OPENVPN_EASYRSA_ROOT}/pki/issued/${client}.crt" ]] && printf "[-] Client name for revocation doesn't exist\n" && exit 2
    cd "${OPENVPN_EASYRSA_ROOT}"
    date -u >>"${OPENVPN_EASYRSA_ROOT}/install.log"
    ./easyrsa --batch revoke "${client}" 2>>"${OPENVPN_EASYRSA_ROOT}/install.log" 1>&2
    ./easyrsa --batch --days=3650 gen-crl 2>>"${OPENVPN_EASYRSA_ROOT}/install.log" 1>&2
    rm -f "${OPENVPN_SERVER_ROOT}/crl.pem"
    cp "${OPENVPN_EASYRSA_ROOT}/pki/crl.pem" "${OPENVPN_SERVER_ROOT}/crl.pem"
    chown nobody:"$group_name" "${OPENVPN_SERVER_ROOT}/crl.pem"
    printf "[*] Revoked client ${client}\n"
}

function setup() {
    if [[ -f /etc/os-release ]]; then
        lsb_dist=$(. /etc/os-release && printf "${ID},${VERSION_ID}")
    fi

    DIST=$(printf "${lsb_dist}" | tr '[:upper:]' '[:lower:]')
    OS=$(printf "${DIST}" | cut -d ',' -f1)
    VERSION_ID=$(printf "${DIST}" | cut -d ',' -f2 | tr -d '.')
    case "${OS}" in
        "raspbian")
            printf "[*] Detected Raspbian ${VERSION_ID}\n"
            group_name="nogroup"
            packages="curl tar gnupg2 openvpn openssl ca-certificates iptables ufw dnsutils"
            printf "[*] Installing packages...\n"
            DEBIAN_FRONTEND=noninteractive apt update -yqq >/dev/null 2>&1
            DEBIAN_FRONTEND=noninteractive apt install -yqq $packages >/dev/null 2>&1
            ;;
        "ubuntu")
            printf "[*] Detected Ubuntu ${VERSION_ID}\n"
            [[ "${VERSION_ID}" -lt 1804 ]] && printf "[-] Unsupported Ubuntu under version 1804, exiting...\n" && exit 2
            group_name="nogroup"
            packages="curl tar gnupg2 openvpn openssl ca-certificates iptables ufw"
            printf "[*] Installing packages...\n"
            DEBIAN_FRONTEND=noninteractive apt update -yqq >/dev/null 2>&1
            DEBIAN_FRONTEND=noninteractive apt install -yqq $packages >/dev/null 2>&1
            ;;
        "debian")
            printf "[*] Detected Debian ${VERSION_ID}\n"
            [[ "${VERSION_ID}" -lt 9 ]] && printf "[-] Unsupported Debian under version 9, exiting...\n" && exit 2
            group_name="nogroup"
            packages="curl tar gnupg2 openvpn openssl ca-certificates iptables ufw"
            printf "[*] Installing packages...\n"
            DEBIAN_FRONTEND=noninteractive apt update -yqq >/dev/null 2>&1
            DEBIAN_FRONTEND=noninteractive apt install -yqq $packages >/dev/null 2>&1
            ;;
        "centos")
            printf "[*] Detected CentOS ${VERSION_ID}\n"
            [[ "${VERSION_ID}" -lt 7 ]] && printf "[-] Unsupported CentOS under  version 7, exiting...\n" && exit 2
            group_name="nobody"
            packages="curl tar openvpn openssl ca-certificates tar firewalld"

            [[ "${VERSION_ID}" -eq 7 ]] && packages+="policycoreutils-python" || packages=+"policycoreutils-python-utils"

            printf "[*] Installing packages...\n"
            yum install -y epel-release
            yum install -y $packages
            ;;
        "fedora")
            printf "[*] Detected Fedora ${VERSION_ID}\n"
            group_name="nobody"
            packages="curl tar openvpn openssl ca-certificates tar firewalld"
            printf "[*] Installing packages...\n"
            dnf install -y "${packages}"
            ;;
    esac

    if systemd-detect-virt -cq; then
        mkdir -p /etc/systemd/system/openvpn-server@server.service.d/
        printf "[Service]\nLimitNPROC=infinity\n" > /etc/systemd/system/openvpn-server@server.service.d/disable-limitnproc.conf
    fi
}

function setup_firewall() {
    if systemctl is-active -q firewalld.service; then
        printf "[*] Detected firewalld, setting zones and rules...\n"
        firewall-cmd --add-port="${port}"/"${protocol}"
        firewall-cmd --zone=trusted --add-source=10.8.0.0/24
        firewall-cmd --permanent --add-port="${port}"/"${protocol}"
        firewall-cmd --permanent --zone=trusted --add-source=10.8.0.0/24
        firewall-cmd --direct --add-rule ipv4 nat POSTROUTING 0 -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j SNAT --to "${host_ip}"
        firewall-cmd --permanent --direct --add-rule ipv4 nat POSTROUTING 0 -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j SNAT --to "${host_ip}"

        systemctl --enable -q now firewalld.service
        if [[ "${port}" != 1194 ]] && sestatus | grep -i "Current mode" | grep -qi "enforcing"; then
            semanage port -a -t openvpn_port_t -p "${protocol}" "${port}"
        fi
    else
        printf "[*] Setting iptables rules and systemd network unit file...\n"
        iptables_path=$(command -v iptables)
        cat > /etc/systemd/system/openvpn-iptables.service <<EOF
[Unit]
Before=network.target

[Service]
Type=oneshot
ExecStartPre=$(which sed) -i 's/net.ipv4.ip_forward.*=.*0/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
ExecStartPre=$(which sed) -i '/.*net.ipv4.ip_forward.*/s/^#//g' /etc/sysctl.conf
ExecStartPre=$(which sysctl) -w net.ipv4.ip_forward=1
#ExecStartPre=$(which echo) 1 > /proc/sys/net/ipv4/ip_forward
ExecStart=$iptables_path -t nat -A POSTROUTING -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j SNAT --to "${host_ip}"
ExecStart=$iptables_path -I INPUT -p "${protocol}" --dport "${port}" -j ACCEPT
ExecStart=$iptables_path -I FORWARD -s 10.8.0.0/24 -j ACCEPT
ExecStart=$iptables_path -I FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
ExecStop=$iptables_path -t nat -D POSTROUTING -s 10.8.0.0/24 ! -d 10.8.0.0/24 -j SNAT --to "${host_ip}"
ExecStop=$iptables_path -D INPUT -p "${protocol}" --dport "${port}" -j ACCEPT
ExecStop=$iptables_path -D FORWARD -s 10.8.0.0/24 -j ACCEPT
ExecStop=$iptables_path -D FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target  
EOF
        systemctl enable --now -q ufw.service
        systemctl enable --now -q openvpn-iptables.service
        systemctl enable --now -q openvpn-server@server.service
    fi

    if [[ $(command -v ufw) ]]; then
        default_iface=$(ip route | awk '/default/ { print $5 }')
        sed -i 's/DEFAULT_FORWARD_POLICY="DROP"/DEFAULT_FORWARD_POLICY="ACCEPT"/g' /etc/default/ufw
        sed -i.old "1s;^;*nat\n:POSTROUTING ACCEPT [0:0]\n-A POSTROUTING -s 10.8.0.0/24 -o "${default_iface}" -j MASQUERADE\nCOMMIT\n\n;" /etc/ufw/before.rules
    fi
}

function setup_pki() {
    printf "[*] Downloading EasyRSA\n"
    mkdir -p "${OPENVPN_EASYRSA_ROOT}"
    curl -sSkL "${EASYRSA_RELEASE}" | tar zxf - --strip 1 -C "${OPENVPN_EASYRSA_ROOT}"
    chown -R root:root "${OPENVPN_EASYRSA_ROOT}"
    cd "${OPENVPN_EASYRSA_ROOT}"
    date -u >>"${OPENVPN_EASYRSA_ROOT}/install.log"

    printf "[*] Generating PKI, CA, server + client certificates\n"
    ./easyrsa --batch init-pki 2>>"${OPENVPN_EASYRSA_ROOT}/install.log" 1>&2
    ./easyrsa --batch build-ca nopass 2>>"${OPENVPN_EASYRSA_ROOT}/install.log" 1>&2

    ./easyrsa --batch --days=3650 build-server-full server nopass 2>>"${OPENVPN_EASYRSA_ROOT}/install.log" 1>&2
    ./easyrsa --batch --days=3650 gen-crl 2>>"${OPENVPN_EASYRSA_ROOT}/install.log" 1>&2
    cp pki/ca.crt pki/private/ca.key pki/issued/server.crt pki/private/server.key pki/crl.pem "${OPENVPN_SERVER_ROOT}"
    chown nobody:"$group_name" "${OPENVPN_SERVER_ROOT}/crl.pem"
    chmod o+x "${OPENVPN_SERVER_ROOT}"
    openvpn --genkey secret "${OPENVPN_SERVER_ROOT}/tc.key"

}

function usage_install() {
    printf -- "Usage $0 [-s <server>] [-p <port>] [-d <dns>]\n\n"
    printf -- "-s      server hosting the OpenVPN server, either an IP or a domain name where DNS resolution will be performed\n"
    printf -- "        recommended to have a dynamic DNS setup\n\n"
    printf -- "-p      port to configure of the OpenVPN server, default 31337\n\n"
    printf -- "-d      OpenVPN DNS resolution, choose from system DNS, Google, Cloudflare, OpenDNS\n"
    printf -- "        the \"system\" option will help with internal network DNS resolution\n"
    printf -- "        however the client will require the resolvconf or openvpn-systemd-resolved packages on linux-based systems\n\n"
    exit 1
}

function usage() {
    printf -- "Usage $0 -a <client to add> | -r <client to revoke> | -u (uninstall)\n\n"
    printf -- "-a      client name to add, creates a certificate\n\n"
    printf -- "-r      client name to revoke, revokes certificate\n\n"
    printf -- "-u      uninstall and remove all OpenVPN configuration files, services etc\n\n"
    exit 1
}

if [[ ! -e "${OPENVPN_SERVER_ROOT}/server.conf" ]]; then
    while getopts "s:p:d:h" option; do
        case $option in 
            s) server=$OPTARG;;
            p) port=$OPTARG;;
            d) dns=$OPTARG;;
            h) usage_install;;
        esac
    done

    if ((OPTIND == 1)); then
        usage_install
    fi

    [[ -z $server ]] && printf -- "-s parameter is mandatory for server configuration\n" && exit 1
    [[ -z $dns ]] && printf -- "-d parameter is mandatory for dns configuration\n" && exit 1

    if [[ $server =~ ^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
        external="$server"
        printf "[*] Using IP $server\n"
    else
        host_ip=$(host $server | grep -Eo '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')
        [[ $? -ne 0 ]] && printf "[-] Could not resolve $1\n"  && exit 2
        external=$server
        printf "[*] $server has IP ${host_ip}\n"
    fi

    if [[ -z $port ]]; then
        port="31337"
        printf "[*] Using default port ${port}\n"
    else
        if [[ "${port}" =~ ^[0-9]+$ && "${port}" -ge 65535 ]]; then
            printf "[-] Invalid port selection, exiting...\n" && exit 2
        fi
        printf "[*] Using port ${port}\n"
    fi

    protocol="udp"
    printf "[*] Using protocol ${protocol}\n"

    if [[ ! -z $dns ]]; then
        dns_opt=$(printf "$dns" | tr '[:upper:]' '[:lower:]')
        case "${dns_opt}" in
            "system")
                printf "[*] Using system DNS resolvers\n"
                gw=$(ip route | awk '/default/ {print $3}')
                server_dns_servers="push \"dhcp-option DNS ${gw}\""$'\n'"push \"dhcp-option DOMAIN-ROUTE .\""$'\n'"push \"dhcp-option DOMAIN $(dnsdomainname)\""
                client_dns_servers="dhcp-option DNS ${gw}"$'\n'"dhcp-option DOMAIN-ROUTE ."
                ;;
            "google")
                printf "[*] Using Google DNS resolvers\n"
                server_dns_servers="push \"dhcp-option DNS 8.8.8.8\""$'\n'"push \"dhcp-option DNS 8.8.4.4\""$'\n'"push \"dhcp-option DOMAIN-ROUTE .\""
                client_dns_servers="dhcp-option DNS 8.8.8.8"$'\n'"dhcp-option DNS 8.8.4.4"$'\n'"dhcp-option DOMAIN-ROUTE ."
                ;;
            "cloudflare")
                printf "[*] Using Cloudflare DNS resolvers\n"
                server_dns_servers="push \"dhcp-option DNS 1.1.1.1\""$'\n'"push \"dhcp-option DNS 1.0.0.1\""$'\n'"push \"dhcp-option DOMAIN-ROUTE .\""
                client_dns_servers="dhcp-option DNS 1.1.1.1"$'\n'"dhcp-option DNS 1.0.0.1"$'\n'"dhcp-option DOMAIN-ROUTE ."
                ;;
            "opendns")
                printf "[*] Using OpenDNS DNS resolvers\n"
                server_dns_servers="push \"dhcp-option DNS 208.67.222.222\""$'\n'"push \"dhcp-option DNS 208.67.220.220\""$'\n'"push \"dhcp-option DOMAIN-ROUTE .\""
                client_dns_servers="dhcp-option DNS 208.67.222.222"$'\n'"dhcp-option DNS 208.67.220.220"$'\n'"dhcp-option DOMAIN-ROUTE ."
                ;;
            *) printf "[-] No preconfigured DNS option selected\n" && exit 1;;
        esac
        printf "[*] Using ${dns_opt} DNS\n"
    else
        server_dns_servers="push \"dhcp-option DNS 8.8.8.8\""$'\n'"push \"dhcp-option DNS 8.8.4.4\""$'\n'"push \"dhcp-option DOMAIN-ROUTE .\""
        client_dns_servers="dhcp-option DNS 8.8.8.8"$'\n'"dhcp-option DNS 8.8.4.4"$'\n'"dhcp-option DOMAIN-ROUTE ."
        printf "[*] Using default Google DNS\n"
    fi

    setup

    if [[ ! -f "${OPENVPN_SERVER_ROOT}/server.key" ]]; then
        setup_pki
    fi

    cat > "${OPENVPN_SERVER_ROOT}/dh.pem" <<EOF
-----BEGIN DH PARAMETERS-----
MIIBCAKCAQEA//////////+t+FRYortKmq/cViAnPTzx2LnFg84tNpWp4TZBFGQz
+8yTnc4kmz75fS/jY2MMddj2gbICrsRhetPfHtXV/WVhJDP1H18GbtCFY2VVPe0a
87VXE15/V8k1mE8McODmi3fipona8+/och3xWKE2rec1MKzKT0g6eXq8CrGCsyT7
YdEIqUuyyOP7uWrat2DX9GgdT0Kj3jlN9K5W7edjcrsZCwenyO4KbXCeAvzhzffi
7MA0BM0oNC9hkXL+nOmFg/+OTxIy7vKBg8P+OxtMb61zO7X8vC7CIAXFjvGDfRaD
ssbzSibBsu/6iGtCOGEoXJf//////////wIBAg==
-----END DH PARAMETERS-----
EOF

    cat > "${OPENVPN_SERVER_ROOT}/server.conf" <<EOF
port $port
proto $protocol
dev tun
ca ${OPENVPN_SERVER_ROOT}/ca.crt
cert ${OPENVPN_SERVER_ROOT}/server.crt
key ${OPENVPN_SERVER_ROOT}/server.key
crl-verify ${OPENVPN_SERVER_ROOT}/crl.pem
tls-crypt ${OPENVPN_SERVER_ROOT}/tc.key
dh ${OPENVPN_SERVER_ROOT}/dh.pem
auth SHA512
tls-version-min 1.2
tls-server
tls-cipher TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256:TLS-ECDHE-ECDSA-WITH-AES-128-GCM-SHA256:TLS-ECDHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA256
topology subnet
server 10.8.0.0 255.255.255.0
sndbuf 0
rcvbuf 0
#script-security 2
#client-connect /etc/openvpn/server/notify.sh
#client-disconnect /etc/openvpn/server/notify.sh
ifconfig-pool-persist ipp.txt
push "redirect-gateway def1 bypass-dhcp"
$server_dns_servers
keepalive 10 120
cipher AES-256-CBC
user nobody
group $group_name
persist-key
persist-tun
# change to 0 to disable logging
verb 3
# uncomment to disable logging
#log /dev/null
#status /dev/null
EOF
    [[ $protocol = "udp" ]] && printf "explicit-exit-notify\n" >> "${OPENVPN_SERVER_ROOT}/server.conf"

    echo 'net.ipv4.ip_forward=1' > /etc/sysctl.d/30-openvpn-forward.conf
    echo 1 > /proc/sys/net/ipv4/ip_forward

    cat > "${OPENVPN_SERVER_ROOT}/client-common.txt" <<EOF
client
dev tun
proto $protocol
remote $external $port
resolv-retry infinite
nobind
persist-key
persist-tun
remote-cert-tls server
tls-version-min 1.2
tls-client
tls-cipher TLS-ECDHE-RSA-WITH-AES-128-GCM-SHA256:TLS-ECDHE-ECDSA-WITH-AES-128-GCM-SHA256:TLS-ECDHE-RSA-WITH-AES-256-GCM-SHA384:TLS-DHE-RSA-WITH-AES-256-CBC-SHA256
auth SHA512
cipher AES-256-CBC
sndbuf 0
rcvbuf 0
$client_dns_servers
verb 3
# install resolvconf or openvpn-systemd-resolved
script-security 2
up /etc/openvpn/update-systemd-resolved # or update-resolv-conf
up-restart
down /etc/openvpn/update-systemd-resolved # or update-resolv-conf
down-pre
EOF

    setup_firewall
    setup_notifications
else
    while getopts "a:r:uh" option; do
        case $option in
            a) new_client $OPTARG;;
            r) revoke_client $OPTARG;;
            u) 
               systemctl disable --now -q openvpn-iptables.service openvpn-server@server.service
               rm -rf /etc/openvpn /etc/systemd/system/openvpn-iptables.service
               printf "[*] Removed openvpn configuration, installation, certificates and services\n"
               ;;
           h) usage;;
        esac
    done

    if ((OPTIND == 1)); then
        usage
    fi
fi
