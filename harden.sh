#!/bin/bash
# -*- coding: utf-8 -*-
set -e

[[ $EUID -ne 0 ]] && printf "Not running as root. Rerun as root\n" && exit 1

SSH_PORT="22"
DATE=$(date +%b_%d_%Y-%H%M%S)

function misc() {
    echo > /etc/issue

    rm -f /etc/cron.deny
    rm -f /etc/at.deny

    touch /etc/cron.allow /etc/at.allow
    chown root:root /etc/cron.allow /etc/at.allow
    chmod 0600 /etc/cron.allow /etc/at.allow
}

function harden_ssh() {
    SSHD_CONF_FILE="/etc/ssh/sshd_config"

    cp $SSHD_CONF_FILE{,.bak-$DATE}
    cat << EOF | sed 's/^ *//' | shuf > "${SSHD_CONF_FILE}"
        Port                            ${SSH_PORT}
        Banner                          /etc/issue
        UsePAM                          yes
        Protocol                        2
        Subsystem                       sftp    /usr/lib/openssh/sftp-server
        LogLevel                        quiet # change to verbose for more
        PrintMotd                       no
        AcceptEnv                       LANG LC_*
        MaxSessions                     5
        StrictModes                     yes
        Compression                     no
        MaxAuthTries                    3
        IgnoreRhosts                    yes
        PrintLastLog                    yes
        AddressFamily                   inet
        X11Forwarding                   no
        PermitRootLogin                 no
        AllowTcpForwarding              no
        ClientAliveInterval             1200
        AllowAgentForwarding            no
        PermitEmptyPasswords            no
        ClientAliveCountMax             0
        GSSAPIAuthentication            no
        KerberosAuthentication          no
        IgnoreUserKnownHosts            yes
        PermitUserEnvironment           no
        ChallengeResponseAuthentication no
        MACs                            hmac-sha2-512,hmac-sha2-256
        Ciphers                         aes128-ctr,aes192-ctr,aes256-ctr
EOF
}

function disable_kernel_modules() {
    BLACKLIST_FILE="/etc/modprobe.d/blacklist.conf"
    [[ ! -d "/etc/modprobe.d" ]] && mkdir -p /etc/modprobe.d
    [[ -f "${BLACKLIST_FILE}" ]] && cp $BLACKLIST_FILE{,.bak-$DATE}
    cat << EOF | sed 's/^ *//' | shuf >> "${BLACKLIST_FILE}"
        blacklist vivid
        blacklist dccp 
        blacklist sctp 
        blacklist rds 
        blacklist tipc 
        blacklist n-hdlc 
        blacklist ax25 
        blacklist netrom 
        blacklist x25 
        blacklist rose 
        blacklist decnet 
        blacklist econet 
        blacklist af_802154 
        blacklist ipx 
        blacklist appletalk 
        blacklist psnap 
        blacklist p8023 
        blacklist p8022 
        blacklist can 
        blacklist atm 
        install vivid /bin/false
        install dccp /bin/false
        install sctp /bin/false
        install rds /bin/false
        install tipc /bin/false
        install n-hdlc /bin/false
        install ax25 /bin/false
        install netrom /bin/false
        install x25 /bin/false
        install rose /bin/false
        install decnet /bin/false
        install econet /bin/false
        install af_802154 /bin/false
        install ipx /bin/false
        install appletalk /bin/false
        install psnap /bin/false
        install p8023 /bin/false
        install p8022 /bin/false
        install can /bin/false
        install atm /bin/false
EOF
}

function whonix_machine_id() {
    printf "b08dfa6083e7567a1921a715000001fb\n" > /etc/machine-id
}

function setup_unattended_upgrades() {
    if [[ $(awk -F= '/^NAME/{print $2}' /etc/os-release | tr -d '"' | tr '[:upper:]' '[:lower:]') == ubuntu ]]; then
        export DEBIAN_FRONTEND=noninteractive
        apt update -yqq && apt install --no-install-recommends -yqq unattended-upgrades
        dpkg-reconfigure --priority=low unattended-upgrades
        UNATTENDED_UPGRADES_CONFIG="/etc/apt/apt.conf.d/50unattended-upgrades"
        if [[ -f $UNATTENDED_UPGRADES_CONFIG ]]; then
            sed -i '/^\/\/.*}-updates";/s/^\/\///'              $UNATTENDED_UPGRADES_CONFIG
            sed -i '/^\/\/.*}-security";/s/^\/\///'             $UNATTENDED_UPGRADES_CONFIG
            sed -i '/^\/\/.*}-apps-security";/s/^\/\///'        $UNATTENDED_UPGRADES_CONFIG
            sed -i '/^\/\/.*}-infra-security";/s/^\/\///'       $UNATTENDED_UPGRADES_CONFIG
            sed -i '/^\/\/.*}-security";/s/^\/\///'             $UNATTENDED_UPGRADES_CONFIG
            sed -i '/^\/\/.*label=Debian-Security";/s/^\/\///'  $UNATTENDED_UPGRADES_CONFIG
            sed -i '/^\/\/.*label=Debian";/s/^\/\///'           $UNATTENDED_UPGRADES_CONFIG
        fi
        AUTOUPDATE_CONFIG=$(grep -il 'Update-Package-Lists' /etc/apt/apt.conf.d/* || printf "/etc/apt/apt.conf.d/99custom-upgrades")
        for i in $AUTOUPDATE_CONFIG; do
            cat > $i << EOF
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::Unattended-Upgrade "1";
APT::Periodic::AutocleanInterval "3";
EOF
        done
        systemctl enable --now -q unattended-upgrades
    else
        printf "Distro is not Ubuntu-based, skipping unattended upgrade configuration\n"
    fi
}

misc
harden_ssh
disable_kernel_modules
whonix_machine_id
setup_unattended_upgrades
