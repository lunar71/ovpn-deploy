#!/bin/bash
# -*- coding: utf-8 -*-

if [[ ! $(command -v wget) ]]; then
    printf "Install wget and rerun\n"
    exit 1
fi

if [[ -z $1 ]]; then
    printf "Usage: $0 <api key>\n"
    exit 1
fi

URL="wget -qO- https://freedns.afraid.org/dynamic/update.php"
UPDATE_URL="${URL}?${1}"

cat > /usr/local/sbin/ddns << EOF
#!/bin/bash
${UPDATE_URL} >> /root/freedns.log 2>&1
EOF
chmod +x /usr/local/sbin/ddns

set -f
echo $(crontab -l; echo '0 * * * * /usr/local/sbin/ddns >/dev/null 2>&1') | crontab -
set +f
